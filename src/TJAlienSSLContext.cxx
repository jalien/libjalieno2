// Author: Volodymyr Yurchenko  28/10/2019

#include "TJAlienSSLContext.h"
#include <iostream>

static int gDebug = 0;
static const char *pass;
static int password_cb(char *buf, int size, int rwflag, void *u);

TJAlienSSLContext::TJAlienSSLContext() { creds.loadCredentials(); }

TJAlienSSLContext::~TJAlienSSLContext() {}

void TJAlienSSLContext::SetCertAndKey(SSL_CTX *ssl_ctx) {
  if (ssl_ctx == nullptr) {
    ERROR("Failed to load cert: ssl context is corrupted");
    return;
  }
  TJAlienCredentialsObject co = creds.get();
  X509 *cert = NULL;
  RSA *rsa = NULL;
  BIO *cbio, *kbio;
  const std::string cert_buffer = co.getCertificate();
  const std::string key_buffer = co.getKey();

  if (cert_buffer != "" && key_buffer != "") {
    cbio = BIO_new_mem_buf((void *)cert_buffer.c_str(), -1);
    cert = PEM_read_bio_X509(cbio, NULL, 0, NULL);
    if (cert == NULL)
      ERROR("PEM_read_bio_X509 failed...");
    SSL_CTX_use_certificate(ssl_ctx, cert);

    pass = co.getPassword().c_str();

    kbio = BIO_new_mem_buf((void *)key_buffer.c_str(), -1);
    rsa = PEM_read_bio_RSAPrivateKey(kbio, NULL, password_cb, NULL);
    if (rsa == NULL)
      ERROR("PEM_read_bio_RSAPrivateKey failed...");
    SSL_CTX_use_RSAPrivateKey(ssl_ctx, rsa);
  }
}

void TJAlienSSLContext::SetCAPath(SSL_CTX *ssl_ctx) {
  if (ssl_ctx == nullptr) {
    ERROR("Failed to set CA's path: ssl context is corrupted");
    return;
  }
  gDebug = std::getenv("gDebug") ? std::stoi(std::getenv("gDebug")) : 0;
  std::string location = "/etc/grid-security/certificates/";
  struct stat info;
  if (stat("/cvmfs/alice.cern.ch", &info) == 0)
    location = "/cvmfs/alice.cern.ch" + location;

  std::string capath = std::getenv("X509_CERT_DIR") ?: location;
  size_t pos = 0;
  std::string token;

  // If capath contans two paths separated by ":"
  while ((pos = capath.find(":")) != std::string::npos) {
    token = capath.substr(0, pos);
    if (gDebug > 1)
      INFO("Loading CA's from " << token);
    if (!SSL_CTX_load_verify_locations(ssl_ctx, nullptr, token.c_str())) {
      if (gDebug > 1)
        ERROR("Failed to load CA locations");
      return;
    }
    capath.erase(0, pos + 1);
  }

  // If capath is a single path
  if (capath.length() != 0) {
    if (gDebug > 1)
      INFO("Loading CA's from " << capath);
    if (!SSL_CTX_load_verify_locations(ssl_ctx, nullptr, capath.c_str())) {
      if (gDebug > 1)
        ERROR("Failed to load CA locations");
      return;
    }
  }
}

static int password_cb(char *buf, int size, int rwflag, void *u) {
  strcpy(buf, pass);
  return (strlen(pass));
}
