# JAliEn interface library for O2

This library manages client's credentials and decorates SSL context with JAliEn-specific information.

## API overview

- Load **all available credentials** from known locations (env, *$HOME/.globus/*, *$TMPDIR/token**)
```
TJAlienCredentials creds;
creds.loadCredentials();
```

- Sort credentials by their priority and get the **top choise**
```
TJAlienCredentialsObject co;
co = creds.get();
```

- Loop over available credentials
```
while (creds.count() > 0) {
  сo = creds.get();
  ...
  creds.removeCredentials(co.kind);
}
```

- Ask for specific type of credentials
```
if (creds.has(cJOB_TOKEN)) {
  сo = creds.get(cJOB_TOKEN);  
}
```

- Upload credentials and CA certificates (from *$X509_CERT_DIR*, CVMFS, */etc/grid-security/certificates*) into SSL context
```
TJAlienSSLContext context;
context.SetCertAndKey((SSL_CTX*)ssl_ctx);
context.SetCAPath((SSL_CTX*)ssl_ctx);

```

## Dependencies
- OpenSSL
- AliEn Runtime