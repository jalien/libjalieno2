// Author: Volodymyr Yurchenko	28/10/2019

#ifndef ROOT_TJAlienSSLContext
#define ROOT_TJAlienSSLContext

#include "TJAlienCredentials.h"
#include <openssl/ssl.h>
#include <sys/stat.h>

class TJAlienSSLContext {
public:
  TJAlienSSLContext();
  ~TJAlienSSLContext();

  void SetCertAndKey(SSL_CTX *ssl_ctx);
  void SetCAPath(SSL_CTX *ssl_ctx);

private:
  TJAlienCredentials creds;
};
#endif
