// Author: Nikola Hardi   3/6/2019
#ifndef ROOT_TJAlienCredentials
#define ROOT_TJAlienCredentials
#include <cstring>
#include <iostream>
#include <map>
#include <string>
#include <termios.h>

#define __FILENAMEEXT__                                                        \
  (strrchr(__FILE__, '/') ? std::string(strrchr(__FILE__, '/') + 1)            \
                          : std::string(__FILE__))
#define __FILENAME__ __FILENAMEEXT__.substr(0, __FILENAMEEXT__.find('.'))
#define INFO(message)                                                          \
  std::cout << "\rInfo in <" << __FILENAME__ << "::" << __func__               \
            << ">: " << message << std::endl
#define ERROR(message)                                                         \
  std::cerr << "\rError in <" << __FILENAME__ << "::" << __func__              \
            << ">: " << message << std::endl

using std::map;
using std::string;

enum CredentialsKind {
  cNOT_FOUND = -1,
  cJBOX_TOKEN = 0,
  cFULL_GRID_CERT,
  cJOB_TOKEN,
  cOTHER_TOKEN,
};

class TJAlienCredentialsObject {
public:
  string certpath;
  string keypath;
  string password;
  CredentialsKind kind;
  bool autoremove;

  TJAlienCredentialsObject() {}

  TJAlienCredentialsObject(string certpath, string keypath,
                           CredentialsKind kind = cOTHER_TOKEN,
                           bool autoremove = false) {
    this->certpath = certpath;
    this->keypath = keypath;
    this->kind = kind;
    this->autoremove = autoremove;
  };

  void wipe() {
    if (autoremove) {
      int gDebug = std::getenv("gDebug") ? std::stoi(std::getenv("gDebug")) : 0;
      if (gDebug)
        INFO("removing safe files: " << certpath.c_str() << keypath.c_str());
      remove(certpath.c_str());
      remove(keypath.c_str());
    }
  }

  bool exists();
  const string getKey();
  const string getCertificate();
  const string getPassword();
  void readPassword();
};

class TJAlienCredentials {
public:
  TJAlienCredentials();
  ~TJAlienCredentials();

  static string getTmpDir();
  static string getHomeDir();
  void loadCredentials();

  bool has(CredentialsKind kind) const;
  TJAlienCredentialsObject get(CredentialsKind kind) const;
  TJAlienCredentialsObject get();
  void removeCredentials(CredentialsKind kind);
  short count();
  void selectPreferedCredentials();
  CredentialsKind getPreferedCredentials() const;
  const string& getMessages() const;
  bool checkCertValidity(const char *path);

  static const char *ENV_JOBTOKEN_KEY;
  static const char *ENV_JOBTOKEN_CERT;
  static const char *TMP_JOBTOKEN_KEY_FNAME_PREFIX;
  static const char *TMP_JOBTOKEN_CERT_FNAME_PREFIX;

private:
  CredentialsKind preferedCredentials;
  void loadTokenCertificate();
  void loadFullGridCertificate();
  void loadJobTokenCertificate();
  string getUsercertPath();
  string getUserkeyPath();
  string getTokencertPath();
  string getTokenkeyPath();

  string getSafeFilename(const string &prefix);
  void writeSafeFile(const string &filepath, const string &content);

  string tmpdir;
  string homedir;
  string msg;
  map<CredentialsKind, TJAlienCredentialsObject> found_credentials;
};
#endif
